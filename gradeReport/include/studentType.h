#include "course.h"

#ifndef STUDENTTYPE_H
#define STUDENTTYPE_H

class studentType
{
    public:
        studentType(string name, int number, int courses, char paidTuition);
        void printStudentInfo();
        int calculateHours();
        double calculateGPA();
        double calculateBilling(int tuitionRate);
        virtual ~studentType();

    protected:

    private:
        void sortCourses();
        string Name;
        int studentID;
        courseType courseEnrolled[6];
        int hasPaidTuition;
        int numberOfCourses;
};

#endif // STUDENTTYPE_H
