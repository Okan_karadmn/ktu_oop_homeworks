#include <iostream>
#include "course.h"

using namespace std;
courseType::courseType(string cName, string cNumber, char cGrade, int  cCredit)
{
    courseName = cName;
    courseNumber = cNumber;
    courseCredit = cCredit;
    courseGrade = cGrade;
    //ctor
}

void courseType::setCourseInfo(string cName, string cNumber, int cCredit)
{
    courseName = cName;
    courseNumber = cNumber;
    courseCredit = cCredit;
}

void courseType::printCourseInfo(){
   cout << "Course Name : " << courseName << endl;
   cout << "Course Number : " << courseNumber << endl;
   cout << "Course Credit : " << courseCredit << endl;
   cout << "Course Grade : " << courseGrade << endl;
}

int courseType::getCredit(){
   return courseCredit;
}

char courseType::getGrade(){
   return courseGrade;
}

string courseType::getCourseNumber(){
   return courseNumber;
}



courseType::~courseType()
{
    //dtor
}
