#include <iostream>
#include "studentType.h"

using namespace std;

studentType::studentType(string name, int number, int courses, char paidTuition)
{
    Name = name;
    studentID = number;
    numberOfCourses = courses;
    hasPaidTuition = paidTuition;
    //ctor
}

void studentType::printStudentInfo(){
   cout << "Student Name = " << Name << endl;
   cout << "Student ID = " << studentID << endl;
   cout << "Number of Courses Enrolled = " << courseEnrolled << endl;
   cout << "Tuition Paid ? = " << hasPaidTuition << endl;
}

int studentType::calculateHours() {
   int totalCredits = 0;
   for(int i=0; i < numberOfCourses; i ++ )
     totalCredits += courseEnrolled[i].getCredit();
   return totalCredits;
}

double studentType::calculateGPA(){

    double sum = 0.0;

    for(int i = 0; i < numberOfCourses; i ++)
    {
        switch(courseEnrolled[i].getGrade()){
        case 'A' :
            sum += courseEnrolled[i].getCredit()*4;
            break;
        case 'B' :
            sum += courseEnrolled[i].getCredit()*3;
            break;
        case 'C' :
            sum += courseEnrolled[i].getCredit()*2;
            break;
        case 'D' :
            sum += courseEnrolled[i].getCredit()*1;
            break;
        case 'F' :
            sum += courseEnrolled[i].getCredit()*0;
            break;
        default:
            break;
        }
    }

    return sum/calculateHours();
}

double studentType::calculateBilling(int tuitionRate){
    return tuitionRate * calculateHours();
}

studentType::~studentType()
{
    //dtor
}
